# Script defines scalar parameters drawn from literature
#
# These are generally single numbers drawn from scientific literature
# for which raw data is either unavailable or un-needed. 
# 
# (The complement to this are calculation variables that, because of spatial variation
# or additional detail, are downloaded in some vector format and loaded/manipulated as dataframes.)

scalars <- list(
  VOLY = (40000/1.25)/1000000, # 2006 euros / 1.25 euro per dollar / then expressed in millions
  DW = list(colorectalCancer = .43), #add more into this sublist
  YLD = list(colorectalCancer = 72-67), # median age at death - median age at diagnosis, average across all races across all genders
  DMC = list(colorectalCancer = 137846 / 1000000) # 2017 dollars, in Millions per yearAC
)

