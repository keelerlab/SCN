## Intro to the project
This readme is focused narrowly on technical documentation of the python repository. 

For more information on the status of nitrate-attributable health cost estimates by public water supply in Minnesota, please consult pdf of summary memo in this repository. 

Details not addressed in this readme or in the pdf memo and its associated folders may be addressed in comments in the scripts themselves. 

## To run on your machine
This repository assumes you have access to the shared drive `SCNdata` and have mounted that drive locally. 
The `set_paths.py` code establishes three directories used through the script on this assumption:
- rawDir = `"/Volumes/GoogleDrive/Shared drives/SCNdata/raw"`
- inDir = `"/Volumes/GoogleDrive/Shared drives/SCNdata/input"`
- outDir = `"/Volumes/GoogleDrive/Shared drives/SCNdata/output"`

## Short outline
The code is structured so the analysis can be reproduced by simply running the `main.py` script. 

This overarching script draws on a few modular pieces which can be edited in future analysis. 
1. `set_paths.py`, which would be edited if the input/output data structure is changed from a locally mounted google drive
2. `params.py`, which defines all parameters used in the calculations, including both constants and the filepaths for demographic and epidemiological data reported with confidence intervals (presently unused). Sensitivity analysis should focus on perturbing these values. 
3. `n_to_cases.py`, which calculates nitrate-attributable disease cases from public water supply nitrate exposure data. Note, this step uses only a subset of the epidemiological research reviewed by this project. Future work can expand this part of the analysis.
4. `cases_to_costs.py`, which calculates hospital, mortality, and morbidity monetary damages by disease and public water supply.  

## Development Software & Packages Used
- Python 3.8.2 in Spyder 5.0.0 IDE 
- pandas 1.2.1
- numpy 1.19.2
