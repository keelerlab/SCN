#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script is the overarching analysis script to produce disease case and 
health cost estimates for public water supplies in Minnesota. 

It calls functions contained within separate python scripts. 

Created on Tue May 18 11:04:30 2021

@author: terinmayer
"""
def main():
    #%% Load requisite libraries
    from datetime import date
    
    #%% Set paths
    """
    Note script assumes that a google drive is locally mounted according to the
    paths stipulated in set_paths.py.
    """
    from set_paths import inDir, outDir, rawDir
    
    #%% Load parameters
    """
    Parameters are set by the params.py file. Sensitivity analysis can be accomplished
    by either importing a different, modified file, or by passing the parameters
    through some additional function. 
    
    Where parameters define an input .csv file, those estimates typically 
    contain confidence intervals, so the required step involves reading in those
    files in a way that allows for resampling from a distribution which can 
    be defined using that statistical information. 
    """
    import params
    
    #%% Set output file names based on run-date of this script
    today = date.today()
    d = today.strftime("%m-%d-%Y")

    #%% N to Cases
    import n_to_cases
    
    # df
    dfCases = n_to_cases.main(pwsDemographics = rawDir + "/" + params.pwsDemoFile, 
            rrTable = inDir + "/" + params.rrTableFile, 
            mdhCancerInfo = rawDir + "/" + params.mdhCancerFile, 
            nihCancerInfo = rawDir + "/" + params.nihCancerFile, 
            peristatsSpinaBifidaInfo = rawDir + "/" + params.peristatsSpinaBifidaFile, 
            peristatsAnencephalyInfo = rawDir + "/" + params.peristatsAnencephalyFile, 
            peristatsVLBWInfo = rawDir + "/" + params.peristatsVLBWFile, 
            peristatsVPTBInfo = rawDir + "/" + params.peristatsVPTBFile,
            propXX = params.propXX,
            propOvaries = params.propOvaries,
            propMtetr = params.propMtetr,
            )
    
    # print df to temp
    dfCases.to_csv(outDir+"/n_attr_cancer_cases_by_pws_"+d+".csv")

    #%% Cases to Costs
    
    import cases_to_costs
    
    # input df in above as argument
    pwsCosts = cases_to_costs.main(df = dfCases,
         VSL = params.VSL,
         dSurvivalRates = params.dSurvivalRates,
         perCaseDirectCost = params.perCaseDirectCost,
         YLD = params.YLD,
         YLL = params.YLL,
         VOLY = params.VOLY)
    # print output

    pwsCosts.to_csv(outDir+"/costs_by_pws_"+d+".csv")    

#%% Run main
if __name__ == "__main__":
    main()