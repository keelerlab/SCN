#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Cases to Costs

This script takes nitrate attributable cases of a number of diseases by PWS 
and executes calculations to output direct and indirect monetary costs.

Dependencies:
    - parameters as defined in main script
    - Inputs a dataframe assumed to be output by n_to_cases function

Output form:
    - A long-formatted dataframe with columns for PWS, disease, case counts, 
    direct costs, indirect_vsl, indirect_QALY

d = {canc_colorectal, canc_ovarian, canc_thyroid, canc_kidney, canc_bladder,
     birth_anencephaly, birth_spinabifida, birth_vpt, birth_vlw}
    ...vpt is very preterm (<32 weeks), vlw is very low weight (<1500g)

Created on Tue Apr 13 11:27:06 2021

@author: terinmayer
"""


#%% import packages
import pandas as pd 

#%% Mortality/Morbidity functions
def calcVSLCost(cases, disease, dSurvivalRates, VSL):
    '''
    Parameters
    ----------
    cases : count of cases of the given disease
    disease : one of th set itemized above
    dSurvivalRates : a dictionary of survival rates, as defined in parameters
    VSL : value of a statistical life, as defined in parameters 

    Returns
    -------
    cost : Monetary cost of premature deaths due to given nitrate-attributable
    disease case. 

    '''
    cost = cases * (1-dSurvivalRates[disease]) * VSL
    return cost
    
def calcQALYCost(cases, disease, YLD, YLL, VOLY):
    '''
    Parameters
    ----------
    cases, disease: as above
    YLD : Disability adjusted life-years
    YLL : Years of life lost
    VOLY : Value of a life year

    Returns
    -------
    cost : quality adjusted life year valuation of morbidity due to given
    nitrate-attributable disease case. 

    '''
    cost = cases * (YLD[disease] + YLL[disease]) * VOLY
    return cost

#%% Main
def main(df,
         VSL,
         dSurvivalRates,
         perCaseDirectCost,
         YLD,
         YLL,
         VOLY):
    '''
    Parameters
    ----------
    df : input dataframe assumed to have structure of the output from n_to_cases
    VSL,  
    dSurvivalRates,
    perCaseDirectCost,
    YLD, 
    YLL, 
    VOLY : As defined in parameters set by main script and then passed through
    to functions called
        
    Returns
    -------
    pwsCosts : a long-format dataframe with costs by disease and public water
    supply

    '''
    # Construct and fill an output data frame
    # list may need editing as new diseases are studied. 
    dList = ['canc_bladder', 'canc_colorectal','canc_kidney',
             'canc_ovarian','canc_thyroid','birth_anencephaly',
             'birth_spinabifida','birth_vpt','birth_vlw']
    dListCaseCol = ['count_n_attr_bladder',
                    'count_n_attr_colorectal',
                    'count_n_attr_kidney',
                    'count_n_attr_ovarian',
                    'count_n_attr_thyroid',
                    'count_n_attr_anencephaly',
                    'count_n_attr_spinabifida',
                    'count_n_attr_vptb',
                    'count_n_attr_vlbw']
    pwsCosts = pd.DataFrame(columns = ['PWS','disease','cases','medical_costs','VSL','QALY'],
                            index = range(len(df) * len(dList)))
    
    # iterate through public water supplies and diseases to calculate costs
    i = 0
    for p in range(len(df)):
        for d in range(len(dList)):
            pwsCosts.iloc[i].loc['PWS'] = df.iloc[p].loc['pwsId']
            pwsCosts.iloc[i].loc['disease'] = dList[d]
            cases = df.iloc[p].loc[dListCaseCol[d]]
            pwsCosts.iloc[i].loc['cases'] = cases
            pwsCosts.iloc[i].loc['medical_costs'] = cases * perCaseDirectCost[dList[d]]
            pwsCosts.iloc[i].loc['VSL'] = calcVSLCost(cases, dList[d], 
                                                      dSurvivalRates = dSurvivalRates,
                                                      VSL = VSL)
            pwsCosts.iloc[i].loc['QALY'] = calcQALYCost(cases, dList[d],
                                                        YLD = YLD,
                                                        YLL = YLL,
                                                        VOLY = VOLY)
            i = i+1    
    
    return pwsCosts