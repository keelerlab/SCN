#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Nitrate to Adverse Health Outcome Cases

This scripts calculates counts
of adverse health outcomes by Public Water Supply (PWS). 

Requires that the following inputs exist in .csv form:
    - risk ratio lookup table with fields for condition (d), 
        exposure group (i), nitrate thresholds (n, assumed to have inclusive 
        lower bound), and risk ratio (r) 
        NOTE: this requirement is based on a working version of the relative
        risk table. Future work should coerce output of John Seng's script
        into this format. 
    - public water supply info with fields for unique PWS identifier (PWSid), 
        population (pop), demographic group (i)
    - condition incidence info from MDH, CDC/NIH, or March of Dimes Peristats
        (samples of which are in SCNdata/raw)

Exposure groups:
i = {xx5569, xx5569wo, births, mtetr, all}
    women 55-69, ... w/o bilateral oophorectomy, babies, above median meat eaters, all

Diseases: 
d = {canc_colorectal, canc_ovarian, canc_thyroid, canc_kidney, canc_bladder,
     birth_anencephaly, birth_spinabifida, birth_vpt, birth_vlw}
    ...vpt is very preterm (<32 weeks), vlw is very low weight (<1500g)
    
(Peristats and Steyner/Matthewson RR use same defs of vpt and vlw.)


Created on Wed Dec 23 09:30:42 2020

@author: terinmayer
"""

#%% import packages
import math
import pandas as pd 
import numpy as np


#%% Key Nitrate Attribution Functions
def getRR(n,d,i, rrTable):
    '''
    Parameters
    ----------
    n : nitrate exposure in mg/l
    d : disease
    i : exposure group
    rrTable : relative risk file from which
        RR values are drawn

    Returns
    -------
    A relative risk value
    '''

    #import the specific relative risk table
    rdf = pd.read_csv(rrTable)
    
    # make number columns numeric, including infinities
    rdf[["n_lo", "n_hi","r"]] = rdf[["n_lo", "n_hi","r"]].apply(pd.to_numeric)
    rdf.replace(to_replace="inf", value=math.inf)
    
    # subset rdf based on disease and population arguments
    tbl = rdf.loc[(rdf.D == d) & (rdf.i ==i)]
    
    # return the relative risk value of the appropriate bin
    out = tbl.loc[(tbl.n_lo <= n) & (n < tbl.n_hi), ['r']]
    return(out.iloc[0]['r'])


def attrCases(n,d,i,I,P, rrTable):
    '''
    Parameters
    ----------
    n, d, i, rrTable : passthrough arguments, same as above
    I : disease incidence
    P : population exposed
    

    Returns
    -------
    Case count.
    '''
    RR = getRR(n, d, i, rrTable)
    C = I * P * (RR - 1) / RR
    return(C)

#%% Data manipulation/cleaning functions

def createWDF(pwsDemographics,
              propXX,
              propOvaries,
              propMtetr): 
    '''
    Parameters
    ----------
    pwsDemographics : input spreadsheet filepath with demographic info by 
        public water supply
    propXX : share of female population
    propOvaries : share of women who retain ovaries
    propMtetr : share of pop eating above median red meat

    Returns
    -------
    wdf : a working data frame, with new and renamed columns
    '''
    df = pd.read_csv(pwsDemographics)
    for col in df.columns: 
        print(col) 
    
    #... create a working copy of the input dataframe that will be manipulated
    # ... to hold output
    wdf = df[["pwsId", 
              "pwsNameLast",
              "county",
              "all_time_max",
              "all_time_mean",
              "avg_annual_max",
              "max_annual_mean",
              "TotPop",
              "Age55_59",
              "Age60_64",
              "Age65_74",
              "WomenGivingBirth"]]
    # ... assignment of sub-pops
    wdf.loc[:,"xx5569"] = (wdf.Age55_59 + wdf.Age60_64 + .5 * wdf.Age65_74) * propXX
    wdf.loc[:,"xx5569wo"] = wdf.xx5569 *  propOvaries
    wdf.loc[:,"mtetr"] = wdf.TotPop * propMtetr
    wdf.rename(columns={"TotPop": "all", "WomenGivingBirth": "births"}, inplace=True)
    # note women giving birth is not *technically* the same as births
    # could adjust based on rate of multiples... 
    return wdf 

def get_cancer_data(mdhCancerInfo, cancer_type, sex='All'):
    '''
    Parameters
    ----------
    mdhCancerInfo : filepath for spreadshee with incidence rates for some of 
        the cancers, in count per 100,000
    cancer_type : name of cancer
    sex : optional argument to subset to females only. The default is 'All'.

    Returns
    -------
    dfout : an output dataframe with county informationt to facilitate joining
    '''
    df = pd.read_csv(mdhCancerInfo)
    df.drop('Unnamed: 13', axis=1, inplace=True)
    dfout = df.loc[(df['type'] == cancer_type) & (df['sex'] == sex)]
    dfout = dfout[["location","rate","count","unstable"]]
    dfout.loc[:,"rate"] = dfout.rate # a string because some have "(UR)" to denote instability
    dfout["rate"] = dfout["rate"].str.rstrip(' (UR)') # remove that string
    dfout["rate"] = dfout["rate"].astype(float) / 100000
    dfout.rename(columns={"rate":"I_"+cancer_type, "count":"count_"+cancer_type, "unstable":"Uflag_"+cancer_type}, inplace=True)
    dfout.rename(columns={"location":"county"}, inplace=True)
    return dfout

def imputeNprep(var, filePath):
    '''
    Parameters
    ----------
    var : either "vptb" or "vlbw", short names for pre-term or low birthweight 
    filePath : filepath to peristats spreadsheet

    Returns
    -------
    A similar spreadsheet but where incidence missing because of small numbers
    is assumed to be the minimum incidence in the state, and where the statewide
    rate is ommitted. Can also be joined by county. 

    '''
    df = pd.read_csv(filePath)
    df.Percent.loc[df.Percent=="**"]=np.nan
    df.Percent = df.Percent.astype(float) / 100
    # where missing, replace with minimum incidence
    # "** Suppressed due to missing data or insufficient numbers."
    df.Percent.loc[df.Percent.isna()] = df.Percent.min(skipna=True)
    # (alternative could be putting in zeros instead...) 
    df.rename(columns={"Region":"county", "Percent":"I_"+var}, inplace=True)
    df = df[df.county != "Minnesota"]
    return(df)

def joinIncidences(wdf, 
                   mdhCancerInfo,
                   nihCancerInfo, 
                   peristatsSpinaBifidaInfo, 
                   peristatsAnencephalyInfo, 
                   peristatsVLBWInfo, 
                   peristatsVPTBInfo):
    '''
    Parameters
    ----------
    wdf : working data frame onto which incidence columns will be joined
    other arguments are filepaths and generally passed through. 

    Returns
    -------
    wdf_merged : inpute data frame but now with incidences
    '''

    
    #Incidences, as dataframes using functions defined above
    Icolorectal = get_cancer_data(mdhCancerInfo, "Colorectal")
    Ithyroid = get_cancer_data(mdhCancerInfo, "Thyroid") 
    Ikidney = get_cancer_data(mdhCancerInfo, "Kidney") 
    Ibladder = get_cancer_data(mdhCancerInfo, "Bladder")
    
    Ivpt = imputeNprep("vptb", peristatsVLBWInfo)
    Ivlw = imputeNprep("vlbw", peristatsVPTBInfo) 
    
    #Join incidences by matching on counties
    wdf_merged = pd.merge(wdf, Icolorectal)
    wdf_merged = pd.merge(wdf_merged, Ithyroid)
    wdf_merged = pd.merge(wdf_merged, Ikidney)
    wdf_merged = pd.merge(wdf_merged, Ibladder)
    wdf_merged = pd.merge(wdf_merged, Ivpt)
    wdf_merged = pd.merge(wdf_merged, Ivlw)
    return wdf_merged


#%% Case attribution 
'''
Note-- feb 2021 version uses table values from matthewson 2019 paper. 
This section will need refactoring to accomodate additional diseases and 
associated demographic groups as epi data is more fully incorporated. 
'''

def calculateCases(wdf_merged,
                   propMtetr,
                   propXX,
                   propOvaries,
                   peristatsAnencephalyInfo,
                   peristatsSpinaBifidaInfo,
                   nihCancerInfo, 
                   rrTable):
    '''
    Parameters
    ----------
    wdf_merged : input dataframe with incidences
    peristatsAnencephalyInfo, 
    peristatsSpinaBifidaInfo,    
    nihCancerInfo: filepaths to info on statewide incidence rate
    other arguments passed through

    Returns
    -------
    wdf_merged : dataframe with case counts by pws

    '''

    pwsN = wdf_merged.shape[0]
    
    # initialize empty columns
    wdf_merged["count_n_attr_colorectal"] = np.nan
    wdf_merged["count_n_attr_thyroid"] = np.nan
    wdf_merged["count_n_attr_kidney"] = np.nan
    wdf_merged["count_n_attr_bladder"] = np.nan
    wdf_merged["count_n_attr_ovarian"] = np.nan
    wdf_merged["count_n_attr_vlbw"] = np.nan
    wdf_merged["count_n_attr_vptb"] = np.nan
    wdf_merged["count_n_attr_anencephaly"] = np.nan
    wdf_merged["count_n_attr_spinabifida"] = np.nan
    
    # Note-- where meat eating comes in, 
    # apply two rates to half the population each?
    # that's what is applied here...
    # also note selection of appropriate nitrate measure based on disease
    for i in range(pwsN):
        # colorectal from total population, half of which are above median meat eaters
        wdf_merged.at[i, "count_n_attr_colorectal"] = \
            attrCases(
                wdf_merged.at[i, "all_time_mean"], "canc_colorectal", "all",
                wdf_merged.at[i, "I_Colorectal"], (wdf_merged.at[i, "all"] * (1-propMtetr)), 
                rrTable = rrTable) + \
            attrCases(
                wdf_merged.at[i, "all_time_mean"], "canc_colorectal", "mtetr",
                wdf_merged.at[i, "I_Colorectal"], (wdf_merged.at[i, "all"] * propMtetr), 
                rrTable = rrTable)
        
        # thyroid from women 55-69
        wdf_merged.at[i, "count_n_attr_thyroid"] = \
            attrCases(
                wdf_merged.at[i, "all_time_mean"], "canc_thyroid", "xx5569",
                wdf_merged.at[i, "I_Thyroid"], wdf_merged.at[i, "xx5569"],
                rrTable = rrTable)
       
        # kidney only from above median meat eaters    
        wdf_merged.at[i, "count_n_attr_kidney"] = \
            attrCases(
                wdf_merged.at[i, "all_time_mean"], "canc_colorectal", "mtetr",
                wdf_merged.at[i, "I_Colorectal"], (wdf_merged.at[i, "all"] * propMtetr),
                rrTable = rrTable)
            
        # bladder from women 55-69
        wdf_merged.at[i, "count_n_attr_bladder"] = \
            attrCases(
                wdf_merged.at[i, "all_time_mean"], "canc_bladder", "xx5569",
                wdf_merged.at[i, "I_Bladder"], wdf_merged.at[i, "xx5569"],
                rrTable = rrTable)
            
        # ovarian from women 55-69 with ovaries, with one statewide incidence
        # extract MN ovarian cancer incidence from NIH/CDC data
        # assumes consistent export format from NIH database... 
        # below could be made more robust by not hard-coding
        Iovarian = pd.read_csv(nihCancerInfo, header=8)
        Iovarian = Iovarian.iat[0,3]
        Iovarian = float(Iovarian) / 100000
        wdf_merged.at[i, "count_n_attr_ovarian"] = \
            attrCases(
                wdf_merged.at[i, "all_time_mean"], "canc_ovarian", "xx5569wo", 
                Iovarian, (wdf_merged.at[i, "all"] * propXX * propOvaries),
                rrTable = rrTable)
        
        # very pre term birth from women giving birth
        wdf_merged.at[i, "count_n_attr_vptb"] = \
            attrCases(
                wdf_merged.at[i, "avg_annual_max"], "birth_vpt", "births", 
                wdf_merged.at[i, "I_vptb"], wdf_merged.at[i, "births"],
                rrTable = rrTable)
        
        # very low birth weight from women giving birth
        wdf_merged.at[i, "count_n_attr_vlbw"] = \
            attrCases(
                wdf_merged.at[i, "avg_annual_max"], "birth_vlw", "births", 
                wdf_merged.at[i, "I_vlbw"], wdf_merged.at[i, "births"],
                rrTable = rrTable)
            
        
        
        # Birth issues (anencephaly, spina bifida)
        # below could be made more robust by not hard-coding 
        # prevalence per 10,000 live births
        
        # anencephaly
        Ianencephaly =  pd.read_csv(peristatsAnencephalyInfo)
        Ianencephaly = Ianencephaly.iat[5,1]
        Ianencephaly = float(Ianencephaly) / 10000
        
        wdf_merged.at[i, "count_n_attr_anencephaly"] = \
            attrCases(
                wdf_merged.at[i, "avg_annual_max"], "birth_neuro", "births", 
                Ianencephaly, wdf_merged.at[i, "births"],
                rrTable = rrTable)
        
        # spinabifida
        Ispinabifida =  pd.read_csv(peristatsSpinaBifidaInfo)
        Ispinabifida = Ispinabifida.iat[5,1]
        Ispinabifida = float(Ispinabifida) / 10000
        wdf_merged.at[i, "count_n_attr_spinabifida"] = \
            attrCases(
                wdf_merged.at[i, "avg_annual_max"], "birth_neuro", "births", 
                Ispinabifida, wdf_merged.at[i, "births"],
                rrTable = rrTable)
            
    return wdf_merged 
        

#%% Main function
def main(
        pwsDemographics, 
        rrTable, 
        mdhCancerInfo, 
        nihCancerInfo, 
        peristatsSpinaBifidaInfo, 
        peristatsAnencephalyInfo, 
        peristatsVLBWInfo, 
        peristatsVPTBInfo,
        propXX,
        propOvaries,
        propMtetr,
        ):
    wdf = createWDF(pwsDemographics, propXX, propOvaries, propMtetr)
    Iwdf = joinIncidences(wdf, 
                   mdhCancerInfo,
                   nihCancerInfo, 
                   peristatsSpinaBifidaInfo, 
                   peristatsAnencephalyInfo, 
                   peristatsVLBWInfo, 
                   peristatsVPTBInfo)
    outDF = calculateCases(Iwdf,
                   propMtetr,
                   propXX,
                   propOvaries,
                   peristatsAnencephalyInfo,
                   peristatsSpinaBifidaInfo,
                   nihCancerInfo,
                   rrTable)
    return outDF