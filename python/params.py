#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This file contains all parameters called in the the health cost calculation 
workflow. These can be altered to allow for sensitivity analysis. 

Created on Tue May 18 11:02:29 2021

@author: terinmayer
"""
import numpy as np

#%% Input file CSVs stored in inDir

rrTableFile = "rr_table_Mathewson1.csv"

#%% Input file CSVs stored in rawDir
pwsDemoFile = "muni_pws_demographics.csv"
mdhCancerFile = "cancer_by_county_MDH.csv"
nihCancerFile = "ovarian_canc_incd_NIH.csv"
peristatsAnencephalyFile = "anencephalus_raceeth_MN_2012-2016_Average_peristats.csv"
peristatsSpinaBifidaFile = "spina_bifida_without_anencephalus_raceeth_MN_2012-2016_Average_peristats.csv"
peristatsVLBWFile = "vlbw_MN_2016-2019_Average_March_of_Dimes_Peristats.csv"
peristatsVPTBFile = "vptb_MN_2016-2019_Average_March_of_Dimes_Peristats.csv"

#%% Constants for n to cases
propXX = .5 # proportion of population who is female
propOvaries = .93 # proportion of women who have not had ovaries removed
propMtetr = .5 # proportion of MN pop who eat above median quantities of red meat

#%% Case to cost parameters
""" 
These numbers derived from Madeleine Hansen-Connell's research  
"""
VSL = 9062071 # in 2017 dollars
VOLY = 59960 # in 2017 dollars

perCaseDirectCost = {
    'canc_bladder': 99292, #specifically for women
    'canc_colorectal': 137846,
    'canc_kidney': 138946,
    'canc_ovarian': 211729,
    'canc_thyroid': np.nan, #missing
    'birth_anencephaly': 0, #b/c condition is fatal
    'birth_spinabifida': 621871, #excludes parent's time
    'birth_vpt': 324500,
    'birth_vlw': 259600
    }

dSurvivalRates = {
    'canc_bladder': 0.769,
    'canc_colorectal': 0.646,
    'canc_kidney': 0.752,
    'canc_ovarian': 0.486,
    'canc_thyroid': 0.983, 
    'birth_anencephaly': 0.000, #b/c condition is fatal
    'birth_spinabifida': .872, #see Wong et al 2008
    # note that deaths are only given for a pooled category for those below
    # consistent with issue of vpt vlw correlation
    'birth_vpt': 0.937, # vpt+vlw pooled deaths / live births vpt
    'birth_vlw': 0.930  # vpt+vlw pooled deaths / live births vlw
    }

YLD = {
       # note this is 
       # years lived with disease * 
       #    disease-specific disability weight
    'canc_bladder': 7 * 0.27,
    'canc_colorectal': 6 * 0.43,
    'canc_kidney': 7 * 0.27,
    'canc_ovarian': 7 * 0.43,
    'canc_thyroid': 22 * 0.27, 
    'birth_anencephaly': np.nan, 
    'birth_spinabifida': np.nan, 
    'birth_vpt': np.nan,
    'birth_vlw': np.nan
       }
YLL = {
       # years of life lost to disease
    'canc_bladder': 2.1,
    'canc_colorectal': 5.6,
    'canc_kidney': 7.6,
    'canc_ovarian': 11.1,
    'canc_thyroid': 8.1, 
    'birth_anencephaly': 78, # U.S. life expectancy in 2017
    'birth_spinabifida': 26, # Strauss commentary on Oakeshott cohort study 
    'birth_vpt': np.nan,
    'birth_vlw': np.nan
       }
