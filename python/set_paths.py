#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Set paths for script

Assumes SCNdata is mounted locally via google drive. 
Development used Google Drive for Mac desktop
Version: 47.0.19.0 (Intel)

Created on Mon Dec 21 11:28:23 2020

@author: terinmayer
"""

rawDir = "/Volumes/GoogleDrive/Shared drives/SCNdata/raw"

inDir = "/Volumes/GoogleDrive/Shared drives/SCNdata/input"

outDir = "/Volumes/GoogleDrive/Shared drives/SCNdata/output"